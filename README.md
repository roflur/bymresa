# BYMresA (Besag - York - Mollié Multiresolution Analysis)
  i.e. Multiresolution Decomposition of Areal Count Data.

# to run the _R_ code in the directory code, one needs to
  *  install the developer version of _mrbsizer_ package via `devtools::install_github("romanflury/mrbsizeR")`, see also https://romanflury.github.io/mrbsizeR/.
  *  install the developer version of _mresa_ from directory sources via `install.packages('sources/mresa_0.1.9000.tar.gz', repos = NULL)`.
  *  run the script _oralcavitycancerexample.R_ to reproduce the figures in the shortpaper.

